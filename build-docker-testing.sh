#!/bin/bash
docker stop jambo-websocket-testing
docker rm jambo-websocket-testing
docker stop jambo-websocket-staging
docker rm jambo-websocket-staging
git pull
docker build --tag jambo-websocket .
docker run -d -p 10196:80 --name jambo-websocket-testing -e ENDPOINT=https://testing.jambosystem.com -e JWSPORT=80 --restart always jambo-websocket
docker run -d -p 10096:80 --name jambo-websocket-staging -e ENDPOINT=https://staging.jambosystem.com -e JWSPORT=80 --restart always jambo-websocket
 
