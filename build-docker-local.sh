#!/bin/bash

podman stop srs-websocket
podman rm srs-websocket
podman build --tag srs-websocket .
podman run -d -p 10081:80 --name srs-websocket --restart always srs-websocket
