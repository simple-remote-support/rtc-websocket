#!/bin/bash

podman stop srs-websocket
podman rm srs-websocket
git pull
podman build --tag jambo-websocket .
podman run -d -p 10096:80 --name jambo-websocket -e ENDPOINT=https://www.jambosystem.com -e JWSPORT=80 --restart always jambo-websocket
