FROM node:18.16.0
RUN mkdir /ws
COPY . /ws/

RUN ls -lah /ws
WORKDIR /ws
RUN npm install
ENTRYPOINT [ "node", "server.js" ]
