let config = null;

function init(cfg) {
  config = cfg;
}

async function authenticateUser(userId, ticket){
  if (config.debug) return true;
  let path = "/api/authenticateUser";
  let args = {
    userId: userId,
    ticket: ticket
  }
  let url = config.endpoint + path;
  let fetchData = {
    method: "POST",
    body: JSON.stringify(args),
    headers: { 'Content-Type': 'application/json' },
  }
  let result = await fetch(url, fetchData);
  return result;
}

function getUserData(userId, ticket, callback){

}

export { authenticateUser, init };

