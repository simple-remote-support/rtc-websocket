"use strict";
import WebSocket, { WebSocketServer } from 'ws';

import * as api from "./api.js";
import types from "../frontend/public/js/types.mjs";

var wss = null;
var config = null;
var signalChannels = {}


function WSS (server, cfg) {
  config = cfg;
  if (wss == null) {
    wss = new WebSocketServer({
      server: server,
      path: "/websocket"
    });
    wss.on("connection", function(webSocket) {
      webSocket.on("message", onMessage);
    });
  }
  return wss;
}

export { WSS }

function onMessage(msg) {
  let ws = this;
  var data = JSON.parse(msg);
  var userId = data.userId;
  var ticket = data.ticket;  
  var channelId = data.channelId;
  this.removeListener("message", onMessage);
  switch (data.t){
    case Types.MsgTypes.SignalChannelRequest:
      createSignalChannel(ws, channelId, userId, ticket);
    break;
    case Types.MsgTypes.SignalChannelDebugRequest:
      createDebugSignalChannel(ws, channelId, userId, ticket)
    break;
    case Types.MsgTypes.StreamDistributorRequest:
      createStreamDistributor(ws, channelId, userId, ticket);
    break;
    default:
      ws.send("Unknown Request.")
      ws.close(); 
  }    
}

async function createSignalChannel(ws, channelId, userId, ticket){
  let result = true; 
  if (result){
    var signalChannel = null;
    if (channelId in signalChannels) { 
      signalChannel = signalChannels[channelId];
      signalChannel.addSocket(ws, userId);
    }
    if (signalChannel == null) signalChannel = new SignalChannel(ws, userId, channelId);
          
  } else {
    ws.send("access denied.")
    ws.close(); 
  }    
}

function createDebugSignalChannel(ws, channelId, userId, ticket){  
  var signalChannel = null;
  if (!config.debug) {
    ws.send("No debug.")
    ws.close(); 
  }
  if (channelId in signalChannels) { 
    signalChannel = signalChannels[channelId];
    signalChannel.addSocket(ws, userId);
  }
  if (signalChannel == null) signalChannel = new SignalChannel(ws, userId, channelId);        
}

function createStreamDistributor(){  
  ws.send("access denied.")
  ws.close(); 
}

function WebSocketHandler(ws, userId, signalChannel){
  this.ws = ws;
  this.userId = userId;
  this.signalChannel = signalChannel;
  this.onMessage = (msg) => {
    let data = null;
    try {
      data = JSON.parse(msg);
    } catch (err) {
      console.log(err);
      return;
    }
    switch (data.t){
      case Types.MsgTypes.RTCChannelOffer:
      case Types.MsgTypes.RTCChannelAnswer:
      case Types.MsgTypes.ICECandidate:
        var wsh = this.signalChannel.webSocketHandlers[data.recieverUserId];
        if (wsh) wsh.send(msg);
      	break;
      default:
        for (let i in this.signalChannel.webSocketHandlers){
          let wsh = this.signalChannel.webSocketHandlers[i];
          if (wsh.userId != this.userId) wsh.send(msg);
        }
    }  
  }
  this.onClose = (e) => {
    this.signalChannel.removeWebSocketHandler(this);
  }
  this.onError = (e) => {
    //console.log("uid: "+ this.userId +" socket onError" + e);  
  }

  ws.on("message", this.onMessage);
  ws.on("close", this.onClose);
  ws.on("error", this.onError);
  this.send = (msg) => {
    this.ws.send(msg);
  }
}

function SignalChannel(ws, userId, channelId) {
  this.webSocketHandlers = {};
  let wsh = new WebSocketHandler(ws, userId, this);
  this.webSocketHandlers[userId] = wsh; 
  this.channelId = channelId;
  this.instanceId = new Date().toISOString();
  this.addSocket = (ws, userId) => {
    let wsh =  new WebSocketHandler(ws, userId, this);
    this.webSocketHandlers[userId] = wsh;
    this.sendChannelData(wsh);
  }
  this.removeWebSocketHandler = (wsh) => {
    delete this.webSocketHandlers[wsh.userId];
    if ( Object.keys(this.webSocketHandlers).length < 1){
      delete signalChannels[channelId];
    }
    let userLeftData = { 
      t: Types.MsgTypes.UserLeft,
      userId: wsh.userId 
    }
    for (let i in this.webSocketHandlers) {
      let webSocketHandler = this.webSocketHandlers[i];
      webSocketHandler.send(JSON.stringify(userLeftData))
    }
  }
  this.getParticipants = () => {
    let participants = [];
    for (let uid in this.webSocketHandlers){
      participants.push(uid);
    }
    return participants;
  }
  this.sendChannelData = (wsh) => {
    let channelData = {
      t: Types.MsgTypes.ChannelData,
      instanceId: this.instanceId,
      participants: this.getParticipants()
    }    
    wsh.send(JSON.stringify(channelData));
  }
  
  signalChannels[channelId] = this;
  this.sendChannelData(wsh);
}

function StreamDistributor(ws, userId, channelId){
  this.webSocketHandlers = {};
  let wsh = new WebSocketHandler(ws, userId, this);
  this.webSocketHandlers[userId] = wsh; 
  this.channelId = channelId;
}
