# RTC Websocket

Websocket for connecting p2p video chat
This project contains the websocket service that is used to connect the WebRTC clients.
Shall be served with NodeJS and made available on

https://[URL]/websocket

## Getting started with development

Install NodeJS and npm on your developer machine.  
  
Install Nodemon

```
npm install -g nodemon
```
Checkout the source code and install its dependencies:
```
cd /path/to/your/working/folder
git pull git@gitlab.com:simple-remote-support/rtc-websocket.git
cd frontend
npm install
```
run RTC websocket with the command 
```
nodemon
```

Open this folder in your favorite html IDE.
