#!/usr/bin/env node
"use strict"; 
import fs from 'fs';
import express from 'express';
import * as api from "./api.js";
import { WSS } from "./web-socket-server.js";

let app = express();

let config = {
  port: 7781,
  endpoint: "localhost",
  debug: false
}

for (let i in process.argv){
  var arg = process.argv[i];
  if (arg.includes("debug")) config.debug = true;
}

console.log(config)

let server = app.listen(config.port, function () {
  console.log('Websocket listening on port ' + config.port + '!')
})

api.init(config);
let wss = WSS(server, config);


